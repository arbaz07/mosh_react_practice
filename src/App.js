import React, { Component } from "react";
import ActiveFriendsComponent from './Components/ActiveFriendComponent';
import InactiveFriendComponent from './Components/InactiveFriendcomponent';
import Counter from './Components/Counter' ;


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      friend: [
        {
          name: "Arbaz",
          active: true,
        },
        {
          name: "Asif",
          active: true,
        },
        {
          name: "Hamza",
          active: false,
        },
      ],
      input: "",
      counterDetails: [
        {
          counterNumber: 1,
          count: 0,
        },
      ],
    };
  }

  handleAddFriend = () => {
    this.setState((currentState) => {
      return {
        friend: currentState.friend.concat([
          { name: this.state.input, active: true },
        ]),
        input: "",
      };
    });
  };

  handleRemoveFriends = (name) => {
    this.setState((currentState) => {
      return {
        friend: currentState.friend.filter((fr) => fr.name !== name),
      };
    });
  };

  onToggleFriend = (name) => {
    this.setState((currentState) => {
      const friendName = currentState.friend.find(
        (friend) => friend.name === name
      );

      return {
        friend: currentState.friend
          .filter((friend) => friend.name !== name)
          .concat([
            {
              name: name,
              active: !friendName.active,
            },
          ]),
      };
    });
  };
  updateInput = (e) => {
    const value = e.target.value;
    this.setState({
      input: value,
    });
  };

  incrementCounter = () => {
    this.setState((previousState) => {
      return {
        counterDetails: previousState.counterDetails.concat({
          counterNumber:
            previousState.counterDetails[
              previousState.counterDetails.length - 1
            ].counterNumber + 1,
          count: 0,
        }),
      };
    });
  };

  resetAllCounter = () => {
    this.setState((previousState) => {
      return {
        counterDetails: previousState.counterDetails.map((counter, index) => {
          return {
            counterNumber: index + 1,
            count: 0,
          };
        }),
      };
    });
  };

  increaseCount = (num) => {
    this.setState((previousState) => {
      return previousState.counterDetails.map((counter) => {
        if (counter.counterNumber == num) {
          counter.count = counter.count + 0.5;
        } else {
          return counter;
        }
      });
    });
  };

  resetCounter = (num) => {
    this.setState((previousState) => {
      return previousState.counterDetails.map((counter) => {
        if (counter.counterNumber == num) {
          counter.count = 0;
        } else {
          return counter;
        }
      });
    });
  };

  removeCounter = (num) => {
    this.setState((previousState) => {
      // console.log('previousState :>> ', );
      return {
        counterDetails: previousState.counterDetails.filter(
          (counter) => counter.counterNumber !== num
        ),
      };
    });
  };

  render() {
    // console.log("render");
    return (
      <div className="container">
        <input
          type="text"
          placeholder="New Friend"
          value={this.state.input}
          onChange={this.updateInput}
        />
        <button onClick={this.handleAddFriend}>Submit</button>
        <div>
          <button
            onClick={() =>
              this.setState({
                friend: [],
              })
            }
          >
            clear All
          </button>
        </div>
        <ActiveFriendsComponent
          list={this.state.friend.filter((friend) => friend.active === true)}
          onRemoveFriend={this.handleRemoveFriends}
          onToggle={this.onToggleFriend}
        />
        <InactiveFriendComponent
          list={this.state.friend.filter((friend) => friend.active === false)}
          onRemoveFriend={this.handleRemoveFriends}
          onToggle={this.onToggleFriend}
        />
        <hr></hr>

        <div>
          <h2>{this.state.number}</h2>
          <button onClick={this.incrementCounter}>Add counter</button>
          <button onClick={this.resetAllCounter}>clear all</button>
          <ul className="counterList">
            {this.state.counterDetails.map((counter, index) => {
              return (
                <li key={index}>
                  <Counter
                    counterNumber={counter.counterNumber}
                    number={counter.count}
                    increase={this.increaseCount}
                    reset={this.resetCounter}
                    removeCounter={this.removeCounter}
                  />
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default App;

/*




        items: [],
        DataisLoaded: false,
        countryToDisplay:[]







  componentDidMount() {
    // console.log(`mounted`);
    fetch("https://restcountries.com/v3.1/all")
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json,
          DataisLoaded: true,
          countryToDisplay: json,
        });
      });
  }

  filerByCountry = (name) => {
    name = name.toUpperCase();
    console.log("name :>> ", name);
    this.setState((state) => {
      return {
        countryToDisplay: state.items.filter((country) => {
          let searchName = country.name.common.toUpperCase();
          return searchName.includes(name);
        }),
      };
    });
  };

  filerByRegion = (name) => {
    name = name.toUpperCase();
    console.log("name :>> ", name);
    this.setState((previousState) => {
      if (name == "ALL") {
        console.log("inside if");
        return {
          countryToDisplay: this.state.items,
        };
      } else {
        return {
          countryToDisplay: previousState.items.filter((country) => {
            let searchName = country.region.toUpperCase();
            return searchName.includes(name);
          }),
        };
      }
    });
  };






        <Header
          search={this.filerByCountry}
          searchByRegion={this.filerByRegion}
        />
        <div className="all-countries">
          {this.state.countryToDisplay.map((country, index) => {
            return (
              <CountriesComponent
                key={index}
                imgLink={country.flags.svg}
                countryName={country.name.common}
                countryPopulation={country.population}
                countryRegion={country.region}
                countryCapital={country.capital}
              />
            );
          })}
        </div>



        */
